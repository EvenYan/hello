from django.db import models

# Create your models here.

class ClassInfo(models.Model):
    cname = models.CharField(max_length=20)
    ccapcity = models.IntegerField()
    cstudent = models.IntegerField()
    cpub_date = models.DateTimeField()

    def __str__(self):
        return self.cname

class StudentInfo(models.Model):
    sname = models.CharField(max_length=20)
    sgender = models.BooleanField()
    shobby = models.CharField(max_length=100)
    sbirthday = models.DateTimeField()
    sclass = models.ForeignKey("ClassInfo")

    def __str__(self):
        return self.sname