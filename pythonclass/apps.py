from django.apps import AppConfig


class PythonclassConfig(AppConfig):
    name = 'pythonclass'
