from django.contrib import admin
from pythonclass.models import *
# Register your models here.

class StudentInfoInline(admin.StackedInline):
    model = StudentInfo
    extra = 2

class ClassInfoAdmin(admin.ModelAdmin):
    inlines = [StudentInfoInline]
    list_display = ['pk', 'ccapcity', 'cpub_date']
    fields = ['cname', 'cstudent','ccapcity', 'cpub_date']
    #fieldsets = [
    #    ('basic', {'fields':['cname']}),
    #    ('more', {'fields':['cpub_date']}),
    #]
    list_filter = ['cname']
    search_fields = ['pk']
    list_per_page = 1


def gender(self):
    if self.sgender:
        return "男"
    else:
        return "女"

gender.short_description = "性别"

class StudentInfoAdmin(admin.ModelAdmin):
    list_display = ['sname', gender, 'shobby', 'sbirthday']

admin.site.register(ClassInfo, ClassInfoAdmin)
admin.site.register(StudentInfo, StudentInfoAdmin)

#admin.site.register(ClassInfo, ClassInfoAdmin)
#admin.site.register(StudentInfo)
